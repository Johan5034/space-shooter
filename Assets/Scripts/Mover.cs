﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Mover : MonoBehaviour
    {
        public float Speed;
       
        void Start()
        {
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            rb.velocity = transform.forward*Speed;
        }
    }
}
