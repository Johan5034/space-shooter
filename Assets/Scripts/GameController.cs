﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Vector3 spawnValues;
    public GameObject[] hazards;
    public float hazardcount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    private int score;
    public Text ScoreText;
    public Text GameOverText;
    public Text RestartText;
    private bool gameOver;
    private bool restart;
	// Use this for initialization
	void Start ()
	{
	    gameOver = false;
	    restart = false;
        GameOverText.text = "";
        RestartText.text = "";

        StartCoroutine(SpawnWaves()) ;
	    score = 0;
        UpdateScore();

	}

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
              SceneManager.LoadScene("Main");
            }
        }
    }

    IEnumerator SpawnWaves()
    {

        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardcount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
            if (gameOver==true)
            {
                RestartText.text = "Press 'R' to restart";
                restart = true;
                break;
                
            }
        }
        
       
    }

    public void AddScore(int scoreValue)
    {
        score += scoreValue;
        UpdateScore();
    }

    public void UpdateScore()
    {
        ScoreText.text = "Score: " + score;
    }

    public void GameOver()
    {
        GameOverText.text = "GameOver!";
        gameOver = true;
    }
        
}
