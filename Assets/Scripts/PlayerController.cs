﻿using System.Xml;
using JetBrains.Annotations;

using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Playables;

namespace Assets.Scripts
{
    [System.Serializable]
    public class Boundary
    {
      public float xmin, xmax, zmin , zmax;
    }
    public class PlayerController : MonoBehaviour
    {
        public float tilt;
        public float speed;
        public Boundary boundary;
        public GameObject shot;
        public Transform shotSpawn;
        public float fireRate = 0.5f;
        private float nextFire = 0.0f;
        void Update()
        {
            if (Input.GetKey(KeyCode.F) )
            {
      
                 FireBtn();
            
            }
           
        }
        void FixedUpdate()
        {
            
           
            
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 movement=new Vector3(moveHorizontal, 0.0f, moveVertical);

            Rigidbody rb = gameObject.GetComponent<Rigidbody>();

            rb.velocity = movement*speed;
            
            rb.position=new Vector3(
                Mathf.Clamp(rb.position.x,boundary.xmin, boundary.xmax),
                rb.position.y,
                Mathf.Clamp(rb.position.z, boundary.zmin, boundary.zmax)
                );

           rb.rotation=Quaternion.Euler(0.0f, 0.0f,rb.velocity.x*tilt);
        }



        public void FireBtn()
        {
            if ( Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
               
                Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            }
        }

    }
}
