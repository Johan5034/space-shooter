﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{

    public GameObject Explosion;
    public GameObject PlayerExplosion;
    public int scoreValue;
    private GameController controller;

    void Start()
    {
        GameObject gameControllerObject=GameObject.FindWithTag("GameController");
        if (gameControllerObject!=null)
        {
            controller = gameControllerObject.GetComponent<GameController>();
        }
        if (gameControllerObject==null)
        {
            Debug.Log("Cannot find game controller script");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Hit = " + other.name);
        if (other.tag=="Boundary" || other.tag=="Enemy")
        {
            return;
        }
        if (Explosion!=null)
        {
            Instantiate(Explosion, transform.position, transform.rotation);
        }
        
        if (other.tag=="Player")
        {
            Debug.Log("Player got hit by Enemy ship weapon");
            Instantiate(PlayerExplosion, transform.position, transform.rotation);
            controller.GameOver();
        }
        controller.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
     
    }
}
