﻿using UnityEngine;

namespace Assets.Scripts
{
    public class DestroyByBoundary : MonoBehaviour {

        void OnTriggerExit(Collider other)
        {
            // Destroy everything that leaves the trigger
            Destroy(other.gameObject);
        }


    }
}

