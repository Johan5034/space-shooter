﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{

    public GameObject shot;
    public float delay;
    public float shotRate;
    public Transform shotSpawn;
	// Use this for initialization
	void Start () {
		InvokeRepeating("Fire",delay,shotRate);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Fire()
    {
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        AudioSource audio = gameObject.GetComponent<AudioSource>();
        audio.Play();
    }
}
