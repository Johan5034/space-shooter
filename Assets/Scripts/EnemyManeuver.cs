﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class EnemyManeuver : MonoBehaviour {

        public Vector2 startWait;
        public Vector2 maneuverWait;
        public Vector2 maneuverTime;
        private float targetManeuver;
        public float dodge;
        private Rigidbody rb;
         private float currentSpeed;
        public float smoothing;
        public float tilt;
        public Boundary boundary;


        // Use this for initialization
        void Start()
        {
            rb = gameObject.GetComponent<Rigidbody>();
           
            currentSpeed = (transform.forward*gameObject.GetComponent<Mover>().Speed).z;
            StartCoroutine(Move());
           
        }

        IEnumerator Move()
        {
            yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));
            while (true)
            {
                targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);
                yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
                targetManeuver = 0;
                yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
            }


        }

        void FixedUpdate()
        {
      
            float newManeuver = Mathf.MoveTowards(rb.velocity.x, targetManeuver, Time.deltaTime * smoothing);
            rb.velocity = new Vector3(newManeuver, 0.0f, currentSpeed);
           
            rb.position = new Vector3(
                Mathf.Clamp(rb.position.x, boundary.xmin, boundary.xmax),
                rb.position.y,
                Mathf.Clamp(rb.position.z, boundary.zmin, boundary.zmax)
                );
            rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x*-tilt);


        }
    }
}
