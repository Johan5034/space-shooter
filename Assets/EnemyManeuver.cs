﻿using System.Collections;
using Assets.Scripts;
using UnityEngine;

namespace Assets
{
    public class EnemyManeuver : MonoBehaviour {

        public Vector2 maneuverWait;
        public Vector2 maneuverTime;
        public Vector2 startWait;
        private float targetManeuver;
        public float dodge;
        private Rigidbody rb;
        private float currentSpeed;
        public float smoothing;
        public Boundary boundary;

        // Use this for initialization
        void Start()
        {
            rb = gameObject.GetComponent<Rigidbody>();
            currentSpeed = rb.velocity.x;
            StartCoroutine(Move());
            Debug.Log(currentSpeed);
        }

        IEnumerator Move()
        {
            yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));
            while (true)
            {
                targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);
                yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
                targetManeuver = 0;
                yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
            }


        }

        void FixedUpdate()
        {
            float newManeuver = Mathf.MoveTowards(rb.velocity.x, targetManeuver, Time.deltaTime * smoothing);
            rb.velocity = new Vector3(newManeuver, 0.0f, currentSpeed);
            rb.position = new Vector3(
                Mathf.Clamp(rb.position.x, boundary.xmin, boundary.xmax),
                0.0f,
                Mathf.Clamp(rb.position.z, boundary.zmin, boundary.zmax)
                );
            rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x);


        }
    }
}
